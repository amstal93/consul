<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
	* 1.1. [Details](#Details)
* 2. [Requirements](#Requirements)
* 3. [Examples](#Examples)
	* 3.1. [Create a service in a Swarm](#CreateaserviceinaSwarm)
		* 3.1.1. [Scale up](#Scaleup)
		* 3.1.2. [Scale down](#Scaledown)
		* 3.1.3. [Clean up](#Cleanup)
	* 3.2. [Deploy a Consul stack to a Swarm](#DeployaConsulstacktoaSwarm)
		* 3.2.1. [Scale up](#Scaleup-1)
		* 3.2.2. [Scale down](#Scaledown-1)
		* 3.2.3. [Clean up](#Cleanup-1)
	* 3.3. [Rolling Release](#RollingRelease)
* 4. [Environments](#Environments)
	* 4.1. [Further environments](#Furtherenvironments)
* 5. [Limitations](#Limitations)
* 6. [Links](#Links)
* 7. [Inspired by](#Inspiredby)
* 8. [License](#License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->


# Consul Cluster for Docker Swarm

The image automatically creates a cluster of all running nodes. A manual intervention or creation of the cluster is not necessary.


##  1. <a name='Versions'></a>Versions

The version numbers are separate versions and have no connection with the used applications in the Docker image. In the description you can see the version numbers of the integrated applications.

`latest` [Dockerfile](https://gitlab.com/epicdocker/consul/blob/master/Dockerfile)

`1.9.0` [Dockerfile](https://gitlab.com/epicdocker/consul/blob/release-1.9.0/Dockerfile)

`1.8.0` [Dockerfile](https://gitlab.com/epicdocker/consul/blob/release-1.8.0/Dockerfile)

`1.7.0` [Dockerfile](https://gitlab.com/epicdocker/consul/blob/release-1.7.0/Dockerfile)

`test` [Dockerfile](https://gitlab.com/epicdocker/consul/blob/test/Dockerfile)


###  1.1. <a name='Details'></a>Details

`1.9.0` `latest`

- Used the official [Consul image](https://hub.docker.com/_/consul/) in version `1.4.1` as base image.
- Used the application [ContainerPilot](https://github.com/joyent/containerpilot) in version `3.8.0`.
- Used the template of [Autopilot Pattern](https://github.com/autopilotpattern/consul) for the configuration.

`1.8.0` 

- Used the official [Consul image](https://hub.docker.com/_/consul/) in version `1.4.0` as base image.
- Used the application [ContainerPilot](https://github.com/joyent/containerpilot) in version `3.8.0`.
- Used the template of [Autopilot Pattern](https://github.com/autopilotpattern/consul) for the configuration.

`1.7.0`

- Used the official [Consul image](https://hub.docker.com/_/consul/) in version `1.3.0` as base image.
- Used the application [ContainerPilot](https://github.com/joyent/containerpilot) in version `3.8.0`.
- Used the template of [Autopilot Pattern](https://github.com/autopilotpattern/consul) for the configuration.

`test`

- Test and/or development version - do not use in production.


##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-docker
- Docker Swarm initialized - https://docs.docker.com/engine/swarm/


##  3. <a name='Examples'></a>Examples


###  3.1. <a name='CreateaserviceinaSwarm'></a>Create a service in a Swarm

    docker network create -d overlay consul
    docker service create --name consul --publish 8500:8500 --network consul --replicas 3 --restart-condition any registry.gitlab.com/epicdocker/consul:latest

Once the cluster has been initialized, the web interface `http://localhost:8500/` can be opened in the browser and see the nodes in the cluster.

![consul nodes screenshot](https://gitlab.com/epicdocker/consul/raw/master/images/consul_nodes.png)


####  3.1.1. <a name='Scaleup'></a>Scale up 

    docker service scale consul=5


####  3.1.2. <a name='Scaledown'></a>Scale down

    docker service scale consul=4


####  3.1.3. <a name='Cleanup'></a>Clean up

    docker service rm consul
    docker network rm consul


###  3.2. <a name='DeployaConsulstacktoaSwarm'></a>Deploy a Consul stack to a Swarm

The `simple_cluster.yml` file is located in the subdirectory `examples`.

    docker stack deploy -c simple_cluster.yml consul

After the cluster has been set up, the web interface `http://localhost:8500/` can be opened in the browser.


####  3.2.1. <a name='Scaleup-1'></a>Scale up

    docker service scale consul_cluster=5


####  3.2.2. <a name='Scaledown-1'></a>Scale down

    docker service scale consul_cluster=4


####  3.2.3. <a name='Cleanup-1'></a>Clean up

    docker stack rm consul


###  3.3. <a name='RollingRelease'></a>Rolling Release

See example of a rolling release in the file `examples/rolling_release.md`


##  4. <a name='Environments'></a>Environments

`CONSUL`

*default:* consul

**Important:** The name of the service must always match the property `CONSUL`, otherwise the dynamically assigned IP addresses can not be determined.

`CONSUL_CONFIG_DIR`

*default:* /etc/consul

Path to the configuration directory. The path must be specified absolutely. Only required if you want to use your own configuration file.

`CONSUL_BOOTSTRAP_EXPECT`

*default:* 3

Consul waits until the specified number of servers are available and then bootstraps the cluster.

`CONTAINERPILOT`

*default:* /etc/containerpilot/containerpilot.json5

Path to the configuration file of ContainerPilot. The path must be specified absolutely.


###  4.1. <a name='Furtherenvironments'></a>Further environments

- https://github.com/autopilotpattern/consul#environment-variables
- https://hub.docker.com/_/consul/


##  5. <a name='Limitations'></a>Limitations

- Scaling up of equal or more instances than is set in `CONSUL_BOOTSTRAP_EXPECT` _(default 3)_. It could form a second cluster.
- Scaling down of too many instances can make the cluster inconsistent or affect too many masters.
- Automatic backup for disaster recovery does not exist yet.
- Healthcheck for Consul and ContainerPilot does not exist yet.


##  6. <a name='Links'></a>Links

- Source Code - https://gitlab.com/epicdocker/consul
- GitLab Registry - https://gitlab.com/epicdocker/consul/container_registry
- Docker Hub - https://hub.docker.com/r/epicsoft/consul/
- Anchore Image Overview - https://anchore.io/image/dockerhub/epicsoft%252Fconsul%253Alatest


##  7. <a name='Inspiredby'></a>Inspired by

- ContainerPilot - https://github.com/joyent/containerpilot
- Consul with the Autopilot Pattern - https://github.com/autopilotpattern/consul


##  8. <a name='License'></a>License

MIT License see [LICENSE](https://gitlab.com/epicdocker/consul/blob/master/LICENSE)
